$(function () {
    $('[data-toggle="tooltip"]').tooltip()
    $('[data-toggle="popover"]').popover()
    $('.carousel').carousel({
        interval: 5000
    });
    $('#preguntanos').on('show.bs.modal', function (e){
        console.log('el modal se esta mostrando')
        $('#preguntanosBtn').removeClass('btn-outline-success')
        $('#preguntanosBtn').addClass('btn-primary')
        $('#preguntanosBtn').prop('disabled', true)
        });
    $('#preguntanos').on('shown.bs.modal', function (e){
        console.log('el modal se ha mostrado')
        $('#preguntanosBtn').prop('disabled', false)
        $('#preguntanosBtn').removeClass('btn-primary')
        $('#preguntanosBtn').addClass('btn-outline-success')
    });
    $('#preguntanos').on('hide.bs.modal', function (e){
        console.log('el modal se esta ocultando')
    });
    $('#preguntanos').on('hidden.bs.modal', function (e){
        console.log('el modal fue ocultado')
    });
    })